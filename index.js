#!/usr/bin/env node

const getStdin = require('get-stdin');
const scrape = require('magnet-scrape');

getStdin().then(content => {
  const magnets = scrape(content).map(torrent => torrent.magnet);

  if (magnets.length < 1) {
    console.error('No magnets found!');
    process.exit(1);
  }

  console.log(magnets.join('\n'));
});
