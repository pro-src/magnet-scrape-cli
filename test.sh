#!/bin/bash

set -e
peerflix $(curl -GL 'https://thepiratebay.org/s/?video=on\&page=0\&orderby=99' --data-urlencode "q=$*" | magnet-scrape | head -1)
