# magnet-scrape
CLI wrapper for magnet-scrape 

* [magnet-scrape](https://github.com/pro-src/magnet-scrape) works with any site that contains magnet links in the response!

### Install
```sh
npm i -g magnet-scrape-cli
```

### Usage

```sh
curl https://domain.tld/search?q=example | magnet-scrape | head -1
```

### Advanced Usage

##### Copy link to clipboard: `curl` & `xclip`
```sh
curl -GL 'https://thepiratebay.org/s/?apps=on\&page=0\&orderby=99' --data-urlencode "q=ubuntu desktop" | magnet-scrape | head -1 | xclip -sel clipboard -i
```

##### Download Ubuntu Desktop: `curl` & `aria2`
```sh
aria2c $(curl -GL 'https://thepiratebay.org/s/?apps=on\&page=0\&orderby=99' --data-urlencode "q=ubuntu desktop" | magnet-scrape | head -1)
```

##### Stream videos: `curl` & `peerflix`
```sh
#!/bin/bash

set -e
peerflix $(curl -GL 'https://thepiratebay.org/s/?video=on\&page=0\&orderby=99' --data-urlencode "q=$*" | magnet-scrape | head -1)
```
